#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@library 
@content 
@phase:p1-co1-1 
Feature: Album Content UI

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And that he was able to start with the landing page
  And he was able to login with valid credentials 
  And he was able to create a new library with
    | access      | Public      |
    | libraryType | Album       |
    | name        | My Album    |
    | description | First Album |

Scenario: Create a new album
  Given the test resources exist in the classpath
    | content/sample_photo_a.jpg |
    | content/sample_photo_b.jpg |
    | content/sample_photo_c.jpg |
  When he attempts to add the items to the album 
    | title    | description | resource                   |
    | Sample A |             | content/sample_photo_a.jpg |
    | Sample B |             | content/sample_photo_b.jpg |
    | Sample C |             | content/sample_photo_c.jpg |
  Then he should see that the library has 3 items
  When he attempts to navigate to the "sample-a" content page
  Then he should see that he is on the "johndoe/my-album/sample-a" content page
  And he should see that the content has no comments

