#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@library 
@content 
@phase:p1-co1-1 
Feature: Discussion Content UI

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And that she was able to start with the landing page
  And she was able to login with valid credentials 
  And she was able to create a new library with
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | Discussions    |

Scenario: Create a new discussion
  When she attempts to add the items to the album 
    | title    | description | resource              |
    | Sample A |             | https://www.waweb.io/ |
  Then she should see that the library has 1 items
  When she attempts to navigate to the "sample-a" content page
  Then she should see that he is on the "janedoe/my-discussions/sample-a" content page
  And she should see that the content has no comments
