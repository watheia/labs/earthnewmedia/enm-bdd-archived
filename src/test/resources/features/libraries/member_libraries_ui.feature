#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@library 
@phase:p1-co1-1 
Feature: Member Libraries UI

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And that she was able to start with the landing page
  And she was able to login with valid credentials 
    
Scenario: Create new library 
  When she attempts to create a new library with
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | Discussions    |
  Then she should see that they are on the "janedoe/my-discussions" library page 
  And she should see that the library access is "Public" 
  And she should see that the library type is "Discussion" 
  And she should see that the library name is "My Discussions"
  And she should see that the library description is "Discussions" 
  And she should see that the library was created "Today" 
  And she should see that the delete library button is available 
  
Scenario: Navigate to library page 
  Given she was able to create a new library with 
    | access      | Private     |
    | libraryType | Album       |
    | name        | My Album    |
    | description | Description |
  And she is on the member home page 
  When she attempts to navigate to the "janedoe/my-album" library page 
  Then she should see that they are on the "janedoe/my-album" library page 
  And she should see that the library access is "Private" 
  And she should see that the library type is "Album" 
  And she should see that the library name is "My Album" 
  And she should see that the library was created "Today" 
  And she should see that the library was last modified "Today" 
  
Scenario: Delete library 
  Given she was able to create a new library with 
    | access      | Private  |
    | libraryType | Album    |
    | name        | My Album |
    | description | Description |
  When she attempts to delete the library 
  Then she should see that they are on thier own member home page 
  And she should see that the member library list is empty 
  
Scenario: Edit library access 
  Given she was able to create a new library with 
    | access      | Private     |
    | libraryType | Album       |
    | name        | My Album    |
    | description | Description |
  Then she should see that the library access is "Private"
  When she attempts to change the library access to "Public" 
  And she refreshes the page 
  Then she should see that the library access is "Public"
  