#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@account
@ui 
@phase:p1-co1-1 
Feature: Member Account UI

  As registered Member with a valid account
  I should be able to manage my account information,
  change passwords, and deactivate my account
  
Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And that he was able to start with the landing page
  And he was able to login with valid credentials 
    
Scenario: Navigate to own account page 
  When he attempts to navigate to the member account page 
  Then he should see that they are on the member account page 
  
Scenario: Change member user name (happy path) 
  When he attempts to change the user name to "John Doe" 
  Then he should see that they are on the "john-doe" member account page 
  And he should see that the principal user name is "John Doe"
  And he should see the system info message "User name change successful"
  
Scenario: Change member password (happy path) 
  When he attempts to change the to change the user password to
    | password1 | password2 |
    | password  | password  | 
  Then he should see the system info message "Password change successful" 
  
