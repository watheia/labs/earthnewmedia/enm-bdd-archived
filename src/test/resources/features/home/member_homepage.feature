#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@member_homepage 
@phase:p1-co1-1 
Feature: Member Home Page 

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And that he was able to start with the landing page
  And he was able to login with valid credentials 
    
Scenario: View member home page 
  Then he should see that they are on thier own member home page
  And he should see that the principal user widget is available 
  And he should see that the search widget is available 
  And he should see that the home nav is available 
  And he should see that the account nav is available 
  And he should see that the profile nav is available 
  And he should see that the logout nav is available 
  And he should see that is avatar image is available 
  And he should see that the principal user name is "JohnDoe" 
  And he should see that the create new library widget is available 
  And he should see that the member library list is empty 
