#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@landing_page 
@phase:p1-co1-1 
Feature: Landing Page 

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Bob" is a Guest 
  And that he was able to start with the landing page
    
Scenario: View landing page 
  Then he should see that the home nav is available 
  And he should see that the login nav is available 
  And he should see that the register nav is available 
  And he should see that the search widget is available 
  And he should see that the hero image is available 
