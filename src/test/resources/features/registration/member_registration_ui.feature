#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@ui 
@registration 
@phase:p1-co1-1 
Feature: Member Registration UI 

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Barbra" is a Guest 
  And that she was able to start with the landing page
    
Scenario: Navigate to registration page from landing page 
  When she attempts to navigate to the registration page 
  Then she should see that they are on the registration page 
  And she should see that the registration card is available 
  
Scenario: Register new member account (happy path) 
  Given she was able to navigate to the registration page 
  When she attempts to register an account with 
    | email     | barbra@example.com |
    | userName  | Barbra             |
    | password1 | password3          |
    | password2 | password3          |
  Then she should see that they are on the "barbra" member home page 
  And he should see that the principal user name is "Barbra" 

Scenario: Register new member account (password mismatch) 
  Given she was able to navigate to the registration page 
  When she attempts to enter "barbra@example.com" into the registration form email field
  And she attempts to enter "Barbra" into the registration form user name field
  And she attempts to enter "password3" into the first registration form password field
  And she attempts to enter "oops" into the second registration form password field
  Then she should see the submit member registration button is disabled

@phase:p1-co1-1
Scenario: Register new member account (invalid email) 
  Given she was able to navigate to the registration page 
  When she attempts to register an account with 
    | email     | barbra-example.com |
    | userName  | Barbra             |
    | password1 | password3          |
    | password2 | password3          |
  Then she should see the registration form validation error "Invalid email address" 

@phase:p1-co1-2
Scenario: Register new member account (invalid user name) 
  Given she was able to navigate to the registration page 
  
