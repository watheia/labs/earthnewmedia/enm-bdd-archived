#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@profile
@ui 
@phase:p1-co1-1 
Feature: Member Profile UI

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And that he was able to start with the landing page
  And he was able to login with valid credentials 
    
Scenario: Navigate to own profile page 
  When he attempts to navigate to thier own profile page 
  Then he should see that they are on thier own profile page 
  And he should see that the member attribute list widget is available 
  And he should see that the member attribute list is empty

Scenario: Adding member profile attributes
  A member should be able to add custom attributes to thier profile page.
  Markdown content should be automatically formated as rich text

  Given he is on thier own member profile page
  When he attempts to add "Home Page" to the member attribute list with the content
    """
    https://www.waweb.io
    """
  And he attempts to add "Languages" to the member attribute list with the content
    """
    - Groovy
    - Typescript
    - Rust
    """
  Then he should see that the member attribute list contains a "Home Page" item with the link content "https://www.waweb.io"
  Then he should see that the member attribute list contains a "Languages" item with the list content
    | Groovy     |
    | Typescript |
    | Rust       |
  
