#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@api 
@user 
@attributes 
@phase:p1-co1-1 
Feature: User Attribute API

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And she was able to start using the rest api
  And she was able to login to the api with valid credentials 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And he was able to start using the rest api
  And he was able to login to the api with valid credentials 

Scenario: Create User Attribute API
  When "John" attempts to use the api to create an attribue called "Home Page" with the content
    """
    [waweb.io](https://www.waweb.io/)
    """
  Then he should see that they received a 200 response 
  And the response validates against the "user-attribute" schema 
  And the response has a "markdown" field containing the text 
    """
    [waweb.io](https://www.waweb.io/)
    """
  And the response has an "html" field containing the text 
    """
    <p><a href="https://www.waweb.io/">waweb.io</a></p>
    """

Scenario: Delete User Attribute API (happy path)
  Given "John" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  And he was able to use the api to create an attribue called "bar" with the content
    """
    Bar
    """
  And "Jane" was able to use the api to create an attribue called "bar" with the content
    """
    Bar
    """
  When "John" attempts to delete the user attribute where 
    | label | bar |
  Then he should see that they received a 200 response 
  And the response validates against the "count" schema 
  And the response has a "count" field equal to 1 

@phase:p1-co1-2
Scenario: Delete User Attribute API (authorize for principal only)

Scenario: Count User Attribute API
  Given "John" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  And "Jane" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  When she attempts to count the user attributes where 
    | label | foo | 
  Then she should see that they received a 200 response 
  And the response validates against the "count" schema 
  And the response has a "count" field equal to 2 

Scenario: Find User Attribute API
  Given "John" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  And "Jane" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  When "Jane" attempts to use the api to find the user attributes where
    | label | foo |
  Then she should see that they received a 200 response 
  And the response validates against the "user-attribute-list" schema 
  And the response is a collection with a length of 2

Scenario: Update User Attribute API (happy path)
  Given "John" was able to use the api to create an attribue called "foo" with the content
    """
    Foo
    """
  When he attempts to use the api to update the user attribute with
    | label   | bar |
    | content | Bar |
  Then he should see that they received a 204 response 

@phase:p1-co1-2
Scenario: Update User Attribute API (authorize for principal only)