#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@api 
@user 
@libraries 
@phase:p1-co1-1 
Feature: User Library API

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And he was able to start using the rest api
  And he was able to login to the api with valid credentials 

Scenario: Create Library API
  When "John" attempts to use the api to create a library with 
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | My Description |
  Then he should see that they received a 200 response 
  And the response validates against the "user-library-create" schema
  And the response has a "userId" field equal to the principal id
  And the response has a "slug" field equal to "my-discussions" 
  And the response has an "access" field equal to "Public" 
  And the response has a "libraryType" field equal to "Discussion" 
  And the response has a "name" field equal to "My Discussions" 
  And the response has a "description" field equal to "My Description"
  And the response has a "createdDate" field that has a recent date
  And the response has a "lastUpdate" field that has a recent date

Scenario: Update Library API
  Given he was able to use the api to create a library with 
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | My Description |
  When he attemps to use the api to update the library with
    | access      | Private         |
    | libraryType | Album           |
    | name        | My Album        |
    | description | The Description |
  Then he should see that they received a 200 response 
  And the response validates against the "user-library-update" schema
  And the response has a "userId" field equal to the principal id
  And the response has a "slug" field equal to "my-album"
  And the response has an "access" field equal to "Private" 
  And the response has a "libraryType" field equal to "Album" 
  And the response has a "name" field equal to "My Album" 
  And the response has a "description" field equal to "The Description"
  And the response has a "lastUpdate" field that that has a date later than the "createdDate" value

Scenario: Delete Library API
  Given he was able to use the api to create a library with 
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | My Description |
  And he was able to use the api to create a library with 
    | access      | Private         |
    | libraryType | Album           |
    | name        | My Album        |
    | description | The Description |
  When he attemps to use the api to delete the libraries where
    | slug | my-discussions |
  Then he should see that they received a 200 response 
  And the response validates against the "count" schema
  And the response has a "count" field equal to 1 
