#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@auth
@api 
@phase:p1-co1-1 
Feature: API Authentication 
    
Scenario: Sign in with valid admin account 
  Given that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And she was able to start using the rest api 
  When she attempts to login to the api with valid credentials 
  Then she should see that they received a 200 response 
  And the response validates against the "auth-login" schema
  
Scenario: Allow restricted api call with valid credentials 
  Given that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And he was able to start using the rest api
  And he was able to login to the api with valid credentials 
  When he attempts to GET "/users/me" 
  Then he should see that they received a 200 response 
  And the response has a "userName" field equal to "JohnDoe" 
  And the response has an "email" field equal to "john@example.com" 
    
Scenario: Deny restricted api call 
  Given that "Bob" is a Guest
  And he was able to start using the rest api
  When he attempts to GET "/users/me" 
  Then he should see that they received a 401 response
    