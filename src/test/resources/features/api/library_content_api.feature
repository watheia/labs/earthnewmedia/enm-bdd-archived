#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@api 
@user 
@libraries 
@content 
@phase:p1-co1-1 
Feature: Library Content API

Background: 
  Given that an admin was able to initialize the system under test 
  And that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And she was able to start using the rest api 
  And she was able to login to the api with valid credentials 

Scenario: Create Content API
  Given "Jane" was able to use the api to create a library with 
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | My Description |
  When she attempts to use the api to create a discussion item with 
  | title         | My Blog Post          |
  | description   | My Descripton         |
  | metadata.uri  | https://www.waweb.io/ |
  Then she should see that they received a 200 response 
  And the response validates against the "library-content-create" schema

Scenario: Load Content API
  Given that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And he was able to start using the rest api
  And he was able to login to the api with valid credentials 
  Given "Jane" was able to use the api to create a library with 
    | access      | Public         |
    | libraryType | Discussion     |
    | name        | My Discussions |
    | description | My Description |
  And she was able to use the api to create a discussion item with 
  | title         | My Blog Post          |
  | description   | My Descripton         |
  | metadata.uri  | https://www.waweb.io/ |
  When "John" attempts to use the api to load the content "janedoe/my-discussions/my-blog-post"
  Then he should see that they received a 200 response 
  And the response validates against the "library-content-get" schema


