#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@user 
@api 
@phase:p1-co1-1 
Feature: User API

  The user api should be responsible for actions that affect account
  status of the User entity itself, such as new user registration, 
  changing the account name, adding/changing emails, credentials 
  management, and account deactivation

Background:
  Given that an admin was able to initialize the system under test 

Scenario: Create User API
  Given that "Barbra" is a Guest
  And she was able to start using the rest api
  When she attempts to use the api to create a user with 
    | email    | barbra@example.com |
    | userName | Barbra             |
    | password | barbra1234         |
  Then he should see that they received a 200 response 
  And the response validates against the "user-create" schema

Scenario: Delete User API
  Given that "Joe" is an Admin with the account data 
    | slug     | administrator    |
    | email    | admin@example.com |
    | userName | Administrator    |
    | password | password0        |
  And he was able to start using the rest api 
  And he was able to login to the api with valid credentials 
  When he attemps to delete the users where
    | email | john@example.com |
  Then he should see that they received a 200 response 
  And the response validates against the "count" schema
  And the response has a "count" field equal to 1 

# Scenario: Change User Password API
#   Given that "Jane" is a Member who may call the rest api 
#   And she was able to login to the api with the credentials 
#     | email    | jane@example.com  |
#     | password | password3         |
#   When she attempts to use the api to change the user password to "jane123"
#   Then she should see that the response validates against the "user-password-update" schema

# Scenario: Change User Email API
#   Given that "John" is a Member who may call the rest api 
#   And he was able to login to the api with the credentials 
#     | email    | john@example.com  |
#     | password | password2         |
#   When he attempts to use the api to change the user email to "johndoe@example.com"
#   Then he should see that the response validates against the "user-email-update" schema

# @phase:p1-co1-2 
# Scenario: Add Secondary User Email API
#   Given that "Jane" is a Member who may call the rest api 
#   And she was able to login to the api with the credentials 
#     | email    | jane@example.com  |
#     | password | password3         |
#   When she attempts to use the api to add a secondary user email "janedoe@example.com"
#   Then she should see that the response validates against the "user-email-create" schema

# @phase:p1-co1-2 
# Scenario: Deactivate User Account API
#   Given that "John" is a Member who may call the rest api 
#   And he was able to login to the api with the credentials 
#     | email    | john@example.com  |
#     | password | password2         |
#   When he attempts to use the api to deactivate the user account
#   Then he should see that the response validates against the "user-deactivation-complete" schema