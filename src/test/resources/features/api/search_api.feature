#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@search 
@api 
@phase:p1-co1-2 
Feature: Search API

  The search api takes in some text, and returns a list of possible Entity matches

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And he was able to start using the rest api
  And he was able to login to the api with valid credentials 

Scenario: Search API - search members by name
  When he attempts to use the api to search for "Doe"
  Then he should see the search results contain the members
  | johndoe |
  | janedoe |

Scenario: Search API - search members by email
  When he attempts to use the api to search for "example.com"
  Then he should see the search results contain the members
  | johndoe |
  | janedoe |

