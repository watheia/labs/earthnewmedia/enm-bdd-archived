#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@auth
@ui 
@phase:p1-co1-1 
Feature: Member Login UI

  As an existing member with an active account
  I should be able to sign in with account credentials
  
Background: 
  Given that an admin was able to initialize the system under test 
  And that "Jane" is a Member with the account data 
    | slug     | janedoe          |
    | email    | jane@example.com |
    | userName | JaneDoe          |
    | password | password3        |
  And that she was able to start with the landing page
  
Scenario: Navigate to login from landing page 
  When she attempts to navigate to the login page 
  Then she should see that they are on the login page 
  And she should see that the login form is available 
  
Scenario: Sign in with valid member account (happy path)
  And she was able to navigate to the login page 
  When she attempts to login with the credentials 
  Then she should see that they are on thier own member home page
  
Scenario: Sign in with valid member account (invalid credentials)
  And she was able to navigate to the login page 
  When she attempts to login with the credentials 
  Then she should see the login form validation error "Invalid user name or password"
  
Scenario: Member logout 
  Given she was able to login with valid credentials 
  When she attempts to logout of the application 
  Then she should see that they are on the landing page 
