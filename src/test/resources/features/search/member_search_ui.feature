#Author: aaron.miller@waweb.io
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@search 
@ui 
@phase:p1-co1-2 
Feature: Search UI

Background: 
  Given that an admin was able to initialize the system under test 
  And that "John" is a Member with the account data 
    | slug     | johndoe          |
    | email    | john@example.com |
    | userName | JohnDoe          |
    | password | password2        |
  And that he was able to start with the landing page
  And he was able to login with valid credentials 

Scenario: Search UI - search members by name
  When he attempts to search for "Doe"
  Then he should see the search results contain the items
    | JohnDoe |
    | JaneDoe |
  When he attempts to navigate to the search result "JaneDoe"
  Then he should see that they are on the "janedoe" member home page

Scenario: Search API - search members by email
  When he attempts to search for "jane@example.com"
  Then he should see the search results contain the items
    | JaneDoe |
  When he attempts to navigate to the search result "JaneDoe"
  Then he should see that they are on the "janedoe" member home page

