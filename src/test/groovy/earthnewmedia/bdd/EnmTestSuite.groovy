/**
 * 
 */
package earthnewmedia.bdd

import org.junit.runner.RunWith

import io.cucumber.junit.CucumberOptions
import net.serenitybdd.cucumber.CucumberWithSerenity



/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = ["pretty"], features = "src/test/resources/features")
class EnmTestSuite {}
