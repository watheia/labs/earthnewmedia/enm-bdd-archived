/**
 * 
 */
package earthnewmedia.bdd.stepdefs


import static earthnewmedia.bdd.ui.RegistrationComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.model.RegistrationData
import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.NavigateTo
import earthnewmedia.bdd.tasks.RegisterMemberAccount
import io.cucumber.java.Before
import io.cucumber.java.PendingException
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actions.Enter
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.waits.WaitUntil
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberRegistrationUiStepDefintions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^she was able to navigate to the registration page$/)
    void she_is_on_the_registration_page() {
        theActor.wasAbleTo(NavigateTo.theRegistrationPage())
    }

    @When(/^s?he attempts to navigate to the registration page$/)
    void attempts_to_navigate_to_the_registration_page() {
        theActor.attemptsTo(NavigateTo.theRegistrationPage())
    }

    @When(/^s?he attempts to register an account with$/)
    void attempts_to_register_an_account_with(Map<String, String> data) {
        theActor.attemptsTo(
                WaitUntil.the(REGISTRATION_CARD, isPresent()),
                RegisterMemberAccount.withModel(new RegistrationData(data))
        )
    }
    
    @When(/^s?he attempts to enter "([^"]*)" into the registration form email field$/)
    void attempts_to_enter_into_the_registration_form_email_field(String email) {
        theActor.attemptsTo(
                Enter.theValue(email).into(REGISTRATION_EMAIL_INPUT)
        )
    }

    @When(/^s?he attempts to enter "([^"]*)" into the registration form user name field$/)
    void attempts_to_enter_into_the_registration_form_user_name_field(String userName) {
        theActor.attemptsTo(
                Enter.theValue(userName).into(REGISTRATION_USER_NAME_INPUT)
        )
    }

    @When(/^s?he attempts to enter "([^"]*)" into the first registration form password field$/)
    void attempts_to_enter_into_the_first_registration_form_password_field(String password) {
        theActor.attemptsTo(
                Enter.theValue(password).into(REGISTRATION_PASSWD_INPUT_1)
        )
    }

    @When(/^s?he attempts to enter "([^"]*)" into the second registration form password field$/)
    void attempts_to_enter_into_the_second_registration_form_password_field(String password) {
        theActor.attemptsTo(
                Enter.theValue(password).into(REGISTRATION_PASSWD_INPUT_2)
        )
    }


    @Then(/^s?he should see that they are on the registration page$/)
    void should_see_that_they_are_on_the_registration_page() {
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith("/#/register")),
                seeThat(the(REGISTRATION_CARD), isPresent())
        )
    }

    @Then(/^s?he should see that the registration card is available$/)
    void should_see_that_the_registration_card_is_available() {
        theActor.shouldEventually(
                seeThat(the(REGISTRATION_CARD), isPresent())
        )
    }
    
    @Then(/^s?he should see the submit member registration button is disabled$/)
    void should_see_the_submit_member_registration_button_is_disabled() {
        theActor.should(
                seeThat(the(REGISTRATION_SUBMIT_BTN), not(isEnabled()))
        )
    }


    @Then(/^s?he should see the registration form validation error "([^"]*)"$/)
    void should_see_the_form_validation_error(String expectedText) {
        throw new PendingException()
    }
}
