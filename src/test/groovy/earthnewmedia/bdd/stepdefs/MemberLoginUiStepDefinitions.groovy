/**
 * 
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.LoginComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the
import static org.hamcrest.Matchers.endsWith

import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.LoginWithUi
import earthnewmedia.bdd.tasks.NavigateTo
import io.cucumber.java.Before
import io.cucumber.java.PendingException
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Shared

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberLoginUiStepDefinitions {

    @Shared
    TheActor theActor

    @Before
    void set_the_stage() {
        OnStage.setTheStage(new OnlineCast())
    }
    
    @Given(/^s?he was able to navigate to the login page$/)
    void was_able_to_navigate_to_the_login_page() {
        theActor.wasAbleTo(NavigateTo.theLoginPage())
    }

    @When(/^s?he attempts to navigate to the login page$/)
    void attempts_to_navigate_to_the_login_page() {
        theActor.attemptsTo(NavigateTo.theLoginPage())
    }

    @When(/^s?he attempts to login with the credentials$/)
    void attempts_to_login_with_the_credentials() {
        theActor.wasAbleTo(NavigateTo.theLoginPage())
        theActor.attemptsTo(LoginWithUi.usingCredentials())
    }

    @When(/^s?he attempts to logout of the application$/)
    void attempts_to_logout_of_the_application() {
        theActor.attemptsTo(NavigateTo.theLogoutPage())
    }

    @Then(/^s?he should see that the login form is available$/)
    void should_see_that_the_login_form_is_available() {
        theActor.shouldEventually(
                seeThat(the(LOGIN_FORM), isPresent())
        )
    }

    
    @Then(/^s?he should see that they are on the login page$/)
    void should_see_that_actor_is_on_the_login_page() {
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith("/#/login"))
        )
    }
    
    @Then(/^s?he should see the login form validation error "([^"]*)"$/)
    void should_see_the_login_form_validation_error(String arg1) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    
}
