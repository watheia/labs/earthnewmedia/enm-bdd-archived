/**
 * 
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.PrincipalUserComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.NavigateTo
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.waits.WaitUntil
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberProfileUiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When(/^s?he attempts to navigate to thier own profile page$/)
    void attempts_to_navigate_to_his_profile_page() {
        theActor.attemptsTo(
                WaitUntil.the(PRINCIPAL_CARD, isPresent()),
                NavigateTo.ownProfilePage()
        )
    }

    @Then(/^s?he should see that they are on thier own profile page$/)
    void should_see_that_he_is_on_his_profile_page() {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def expectedUrl = "/#/u/${principal.slug}/profile"
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith(expectedUrl))
        )
    }
    
    @Then(/s?he should see that the member attribute list widget is available$/)
    void should_see_that_the_member_attribute_list_widget_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then(/s?he should see that the member attribute list is empty$/)
    void should_see_that_the_member_attribute_list_is_empty() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Given(/s?he is on thier own member profile page$/)
    void is_on_thier_own_member_profile_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @When(/s?he attempts to add "([^"]*)" to the member attribute list with the content$/)
    void attempts_to_add_to_the_member_attribute_list_with_the_content(String string, String docString) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then(/s?he should see that the member attribute list contains a "([^"]*)" item with the link content "([^"]*)"$/)
    void should_see_that_the_member_attribute_list_contains_a_item_with_the_link_content(String string, String string2) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then(/s?he should see that the member attribute list contains a "([^"]*)" item with the list content$/)
    void should_see_that_the_member_attribute_list_contains_a_item_with_the_list_content(String string, io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException();
    }
}
