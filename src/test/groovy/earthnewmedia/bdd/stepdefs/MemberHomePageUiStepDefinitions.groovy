/**
 * 
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.PrincipalUserComponent.*
import static earthnewmedia.bdd.ui.library.CreateLibraryComponent.*
import static earthnewmedia.bdd.ui.library.MemberLibraryListComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.questions.TheLibraryList
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.NavigateTo
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.questions.targets.TheTarget
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberHomePageUiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage() {
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^s?he is on the member home page$/)
    void is_on_the_member_home_page() {
        theActor.wasAbleTo(NavigateTo.ownHomePage())
    }

    @Then(/^s?he should see that the principal user widget is available$/)
    void should_see_that_the_principal_user_widget_is_available() {
        theActor.shouldEventually(
                seeThat(the(PRINCIPAL_CARD), isPresent())
        )
    }

    @Then(/^s?he should see that the account nav is available$/)
    public void should_see_that_the_account_nav_is_available() {
        theActor.shouldEventually(
                seeThat(the(PRINCIPAL_ACCOUNT_NAV), isPresent())
        )
    }

    @Then(/^s?he should see that the profile nav is available$/)
    public void should_see_that_the_profile_nav_is_available() {
        theActor.shouldEventually(
                seeThat(the(PRINCIPAL_PROFILE_NAV), isPresent())
        )
    }

    @Then(/^s?he should see that the logout nav is available$/)
    public void should_see_that_the_logout_nav_is_available() {
        theActor.shouldEventually(
                seeThat(the(PRINCIPAL_LOGOUT_NAV), isPresent())
        )
    }

    @Then(/^s?he should see that is avatar image is available$/)
    public void should_see_that_is_avatar_image_is_available() {
        theActor.shouldEventually(
                seeThat(the(PRINCIPAL_AVATAR_IMG), isPresent())
        )
    }

    @Then(/^s?he should see that the principal user name is "([^"]*)"$/)
    public void hould_see_that_the_principal_user_name_is(String expectedName) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(PRINCIPAL_USERNAME), equalTo(expectedName))
        )
    }
    
    @Then(/^s?he should see that they are on the "([^"]*)" member home page$/)
    void should_see_that_they_are_on_the_member_home_page(String slug) {
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith("/#/u/${slug}"))
        )
    }
    
    @Then(/^s?he should see that they are on thier own member home page$/)
    void should_see_that_they_are_on_own_member_home_page() {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith("/#/u/${principal.slug}"))
        )
    }
    
    @Then(/^s?he should see that the create new library widget is available$/)
    void should_see_that_the_create_new_library_widget_is_available() {
        theActor.shouldEventually(
                seeThat(the(CREATE_LIBRARY_CARD), isPresent())
        )
    }

    @Then(/^s?he should see that the member library list is empty$/)
    void should_see_that_the_member_library_list_is_empty() {
        theActor.shouldEventually(
                seeThat(the(MEMBER_LIBRARY_LIST), isPresent()),
                seeThat(TheLibraryList.items(), is(empty()))
        )
    }

}
