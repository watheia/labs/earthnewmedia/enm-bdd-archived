/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs.api

import static earthnewmedia.bdd.util.ApiQuery.whereQuery

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.model.Library
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.api.Api
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Steps

/**
 *
 */
class UserLibraryApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^s?he was able to use the api to create a library with$/)
    void was_able_to_use_the_api_to_create_a_library_with(Map<String, String> data) {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def resource = "/users/${principal.id}/libraries"
        
        theActor.wasAbleTo(Api.post().to(resource).withPayload(new Library(data)))
    }

    @Given(/^"([^"]*)" was able to use the api to create a library with$/)
    void actor_was_able_to_use_the_api_to_create_a_library_with(String actor, Map<String, String> data) {
        def principal = Authenticate.asPrincipal(theActor.whoIsNamed(actor).inTheSpotlight())
        def resource = "/users/${principal.id}/libraries"
        
        theActor.wasAbleTo(Api.post().to(resource).withPayload(new Library(data)))
    }

    @When(/^s?he attempts to use the api to create a library with$/)
    void attempts_to_use_the_api_to_create_a_library_with(Map<String, String> data) {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def resource = "/users/${principal.id}/libraries"
        
        theActor.attemptsTo(Api.post().to(resource).withPayload(new Library(data)))
    }
    
    @When(/^"([^"]*)" attempts to use the api to create a library with$/)
    void actor_attempts_to_use_the_api_to_create_a_library_with(String actor, Map<String, String> data) {
        def principal = Authenticate.asPrincipal(theActor.whoIsNamed(actor).inTheSpotlight())
        def resource = "/users/${principal.id}/libraries"
        
        theActor.attemptsTo(Api.post().to(resource).withPayload(new Library(data)))
    }

    @When(/^s?he attemps to use the api to update the library with$/)
    void attemps_to_use_the_api_to_update_the_library_with(Map<String, String> data) {
        // get the library created in the previous step and update the specified fields from the data
        def lastresponse = LastResponse.received().answeredBy(theActor.inTheSpotlight())
        def newLibrary = lastresponse.jsonPath().get() as Map
        def payload = newLibrary.collectEntries{ k, v -> [(k): data.containsKey(k)? data.get(k) : v] }
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def resource = "/users/${principal.id}/libraries"
        
        theActor.attemptsTo(Api.patch().to(resource).withPayload(payload))
    }

    @When(/s?he attemps to use the api to delete the libraries where$/)
    void attemps_to_use_the_api_to_delete_the_libraries_where(Map<String, String> filter) {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def resource = "/users/${principal.id}/libraries${whereQuery(filter)}"
        
        theActor.attemptsTo(Api.delete().from(resource))
    }
}
