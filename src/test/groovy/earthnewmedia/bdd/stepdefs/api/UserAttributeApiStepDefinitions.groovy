/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs.api

import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.steps.UserAttributeApiSteps
import earthnewmedia.bdd.tasks.api.Api
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Steps

/**
 *
 */
class UserAttributeApiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Steps
    UserAttributeApiSteps attributeApi

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^"([^"]*)" was able to use the api to create an attribue called "([^"]*)" with the content$/)
    void actor_was_able_to_use_the_api_to_create_an_attribue_with_the_content(String actor, String label, String content) {
        theActor.whoIsNamed(actor)
                .wasAbleTo(attributeApi.create(label, content))
    }

    @Given(/^s?he was able to use the api to create an attribue called "([^"]*)" with the content$/)
    void was_able_to_use_the_api_to_create_an_attribue_with_the_content(String label, String content) {
        theActor.wasAbleTo(attributeApi.create(label, content))
    }

    @When(/^"([^"]*)" attempts to use the api to create an attribue called "([^"]*)" with the content$/)
    void actor_attempts_to_use_the_api_to_create_an_attribue_called_with_the_content(String actor, String label, String content) {
        theActor.whoIsNamed(actor)
                .attemptsTo(attributeApi.create(label, content))
    }

    @When(/^s?he attempts to use the api to create an attribue called "([^"]*)" with the content$/)
    void attempts_to_use_the_api_to_create_an_attribue_called_with_the_content(String label, String content) {
        theActor.attemptsTo(attributeApi.create(label, content))
    }

    @When(/^s?he attempts to delete the user attribute where$/)
    void attempts_to_delete_the_user_attribute_where(Map<String, String> filter) {
        theActor.attemptsTo(attributeApi.delete(filter))
    }

    @When(/^"([^"]*)" attempts to delete the user attribute where$/)
    void actor_attempts_to_delete_the_user_attribute_where(String actor, Map<String, String> filter) {
        theActor.whoIsNamed(actor)
                .attemptsTo(attributeApi.delete(filter))
    }

    @When(/^"([^"]*)" attempts to count the user attributes where$/)
    void actor_attempts_to_count_the_user_attributes_where(String actor, Map<String, String> filter) {
        theActor.whoIsNamed(actor)
                .attemptsTo(attributeApi.count(filter))
    }

    @When(/^s?he attempts to count the user attributes where$/)
    void attempts_to_count_the_user_attributes_where(Map<String, String> filter) {
        theActor.attemptsTo(attributeApi.count(filter))
    }
    
    @When(/^"([^"]*)" attempts to use the api to find the user attributes where$/)
    void actor_attempts_to_use_the_api_to_find_the_user_attributes_where(String actor, Map<String, String> filter) {
        theActor.whoIsNamed(actor)
                .attemptsTo(attributeApi.find(filter))
    }
    
    @When(/^s?he attempts to use the api to find the user attributes where$/)
    void attempts_to_use_the_api_to_find_the_user_attributes_where(Map<String, String> filter) {
        theActor.attemptsTo(attributeApi.find(filter))
    }
    
    @When(/^"([^"]*)"  attempts to use the api to update the user attribute with$/)
    void actor_attempts_to_use_the_api_to_update_the_user_attribute_with(String actor, Map<String, String> attributeData) {
        theActor.whoIsNamed(actor)
                .attemptsTo(attributeApi.update(attributeData))
    }
    
    @When(/^s?he attempts to use the api to update the user attribute with$/)
    void attempts_to_use_the_api_to_update_the_user_attribute_with(Map<String, String> attributeData) {
        theActor.attemptsTo(attributeApi.update(attributeData))
    }

}
