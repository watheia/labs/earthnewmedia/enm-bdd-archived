/**
 * 
 */
package earthnewmedia.bdd.stepdefs.api

import static earthnewmedia.bdd.questions.TheApiResponse.*
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse
import static net.thucydides.core.matchers.dates.DateMatchers.*
import static org.hamcrest.Matchers.*

import org.joda.time.Period

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.api.Api
import earthnewmedia.bdd.tasks.api.LoginWithApi
import earthnewmedia.bdd.util.DateFormat
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.restassured.response.ValidatableResponse
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CommonApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^s?he was able to start using the rest api$/)
    void was_able_to_start_using_the_rest_api() {
        theActor.whoCanUseTheApi()
    }

    @Given(/^s?he was able to login to the api with valid credentials$/)
    void was_able_to_login_to_the_api_with_valid_credentials() {
        theActor.wasAbleTo(LoginWithApi.asPrincipal())
    }

    @When(/^s?he attempts to login to the api with valid credentials$/)
    void attempts_to_login_to_the_api_with_valid_credentials() {
        theActor.attemptsTo(LoginWithApi.asPrincipal())
    }

    @When(/^s?he attempts to GET "([^"]*)"$/)
    void attempts_to_get(String resource) {
        theActor.attemptsTo(Api.get().from(resource))
    }

    @When(/^s?he attempts to POST to "([^"]*)" with the payload$/)
    void attempts_to_post_with_the_payload(String resource, Map<String, String> payload) {
        theActor.attemptsTo(Api.post().to(resource).withPayload(payload))
    }

    @Then(/^s?he should see that they received a (\d+) response$/)
    void should_see_that_they_received_a_response(Integer statusCode) {
        theActor.should(seeThat(theResponseStatus(),
                equalTo(statusCode)))
    }

    @Then(/^the response has an? "([^"]*)" field equal to "([^"]*)"$/)
    void response_has_a_field_equal_to(String field, String expectedValue) {
        theActor.should(seeThat(theResponseBody(field),
                equalTo(expectedValue)))
    }

    @Then(/^the response has an? "([^"]*)" field equal to$/)
    void response_has_a_field_equal_to(String field, Map<String, String> expectedValue) {
        theActor.should(seeThat(theResponseBody(field, Map),
                equalTo(expectedValue)))
    }

    @Then(/^the response has an? "([^"]*)" field with the entries$/)
    void response_has_a_field_with_the_entries(String field, List<Map<String, String>> expectedEntries) {
        theActor.should(seeThat(theResponseBody(field, ArrayList),
                containsInAnyOrder(expectedEntries)))
    }

    @Then(/^the response has an? "([^"]*)" field containing$/)
    void response_has_a_field_containing(String field, List<String> expectedItems) {
        theActor.should(seeThatResponse({ ValidatableResponse response ->
            expectedItems.each { response.body(field, hasItem(it)) }
        }))
    }

    @Then(/^the response has an? "([^"]*)" field equal to the principal id$/)
    void he_response_has_a_field_equal_to_the_principal_id(String field) {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())

        theActor.should(seeThat(theResponseBody(field),
                equalTo(principal.id)))
    }

    @Then(/^the response has an? "([^"]*)" field that has a recent date$/)
    void hould_see_that_the_response_has_a_field_that_has_a_recent_date(String field) {
        theActor.should(seeThat(theResponseBody(field, DateFormat.&parse),
                isCloseTo(new Date(), Period.hours(1))))
    }

    @Then(/^the response has an? "([^"]*)" field that that has a date later than the "([^"]*)" value$/)
    void response_has_a_field_that_that_has_a_date_later_than_the_value(String firstField, String secondField) {
        def lastResponse = LastResponse.received().answeredBy(theActor.inTheSpotlight())
        def expectedDate = DateFormat.parse(lastResponse.jsonPath().getString(secondField))

        theActor.should(seeThat(theResponseBody(firstField, DateFormat.&parse),
                isAfter(expectedDate)))
    }

    @Then(/^the response has an? "([^"]*)" field equal to (\d+)$/)
    void response_has_a_field_equal_to(String field, Integer expectedValue) {
        theActor.should(seeThat(theResponseBody(field, Integer),
                equalTo(expectedValue)))
    }

    @Then(/^the response has an? "([^"]*)" field containing the text$/)
    public void the_response_has_an_field_containing_the_text(String field, String expectedText) {
        theActor.should(seeThat(theResponseBody(field),
                equalToCompressingWhiteSpace(expectedText)))
    }


    @Then(/^the response does not have an? "([^"]*)" field$/)
    void response_does_not_have_a_field(String field) {
        theActor.should(seeThatResponse({ ValidatableResponse response ->
            response.body(not(hasKey(field)))
        }))
    }

    @Then(/the response validates against the "([^"]*)" schema$/)
    void response_validates_against_the_schema(String schemaName) {
        theActor.should(seeThatResponse({ ValidatableResponse response ->
            response.body(matchesJsonSchemaInClasspath("fixtures/schema/${schemaName}.schema.json"))
        }))
    }

    @Then(/^the response is a collection with a length of (\d+)$/)
    void the_response_is_a_collection_with_a_length_of(Integer expectedLength) {
        theActor.should(seeThat(theResponseEntries(),
                hasSize(expectedLength)))
    }
}
