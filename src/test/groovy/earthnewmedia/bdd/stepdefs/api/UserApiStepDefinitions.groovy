/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs.api

import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.api.Api
import earthnewmedia.bdd.util.ApiQuery
import io.cucumber.java.Before
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 *
 */
class UserApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When(/^s?he attempts to use the api to create a user with$/)
    void attempts_to_use_the_api_to_create_a_user_with(Map<String, String> userData) {
        theActor.attemptsTo(Api.post().to("/users").withPayload(userData))
    }

    @When(/^s?he attemps to delete the users where$/)
    void attemps_to_delete_the_users_where(Map<String, String> filter) {
        theActor.attemptsTo(Api.delete().from("/users${ApiQuery.whereQuery(filter)}"))
    }
}
