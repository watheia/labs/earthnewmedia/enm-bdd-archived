/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs.api

import earthnewmedia.bdd.steps.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 *
 */
class CommentApiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^"([^"]*)" was able to use the api to comment on "([^"]*)" with$/)
    void actor_was_able_to_use_the_api_to_comment_on_with(String actor, String contentSlug, String content) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }


    @When(/^"([^"]*)" attempts to use the api to comment on "([^"]*)" with$/)
    void actor_attempts_to_use_the_api_to_comment_on_with(String actor, String slug, String content) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @When(/^"([^"]*)" attempts to use the api to reply to the comment with$/)
    void actor_attempts_to_use_the_api_to_reply_to_the_comment_with(String actor, String content) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }


    @Then(/^the response contains the comments with the text$/)
    void should_see_that_the_response_contains_the_comments_with_the_text(List<String> expectedContents) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }

    @Then(/^the response contains a comment with the text "([^"]*)" that has the replies$/)
    void should_see_that_the_response_contains_a_comment_with_the_text_that_has_the_replies(String commentText, List<String> expectedReplies) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }
}
