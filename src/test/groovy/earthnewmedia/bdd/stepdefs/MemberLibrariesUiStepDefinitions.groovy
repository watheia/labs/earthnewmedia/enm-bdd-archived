/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.library.MemberLibraryComponent.*
import static earthnewmedia.bdd.ui.library.MemberLibraryListComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.model.AccessLevel
import earthnewmedia.bdd.model.Library
import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.questions.MemberLibraryAccess
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.CreateNewLibrary
import earthnewmedia.bdd.tasks.NavigateTo
import earthnewmedia.bdd.tasks.SetLibraryAccess
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.questions.targets.TheTarget
import net.thucydides.core.annotations.Steps

/**
 *
 */
class MemberLibrariesUiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^s?he was able to create a new library with$/)
    void was_able_to_create_a_new_library(Map<String, String> data) {
        theActor.wasAbleTo(CreateNewLibrary.withModel(new Library(data)))
    }

    @When(/^s?he attempts to create a new library with$/)
    void attempts_to_create_a_new_library(Map<String, String> data) {
        theActor.attemptsTo(CreateNewLibrary.withModel(new Library(data)))
    }

    @When(/^s?he attempts to navigate to the "([^"]*)" library page$/)
    void attempts_to_navigate_to_the_library_page(String slug) {
        def (userSlug, librarySlug) = slug.split("/")
        theActor.attemptsTo(NavigateTo.theLibraryPage(librarySlug))
    }

    @When(/^s?he attempts to delete the library$/)
    void attempts_to_delete_the_library() {
        theActor.attemptsTo(Click.on(LIBRARY_DELETE_BTN))
    }

    @When(/^s?he attempts to change the library access to "([^"]*)"$/)
    void attempts_to_change_the_library_access_to(String access) {
        theActor.attemptsTo(SetLibraryAccess.to(access as AccessLevel))
    }
    @Then(/^s?he should see that "([^"]*)" is in the member library list$/)
    void should_see_that_is_in_the_member_library_list(String slug) {
        def (userSlug, librarySlug) = slug.split("/")
        theActor.shouldEventually(
                seeThat(the(memberLibraryListItem(librarySlug)), isPresent())
        )
    }

    @Then(/^s?he should see that they are on the "([^"]*)" library page$/)
    void should_see_that_they_are_on_the_library_page(String slug) {
        def (userSlug, librarySlug) = slug.split("/")
        def expectedUrl = "/#/u/${userSlug}/lib/${librarySlug}"
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith(expectedUrl))
        )
    }

    @Then(/^s?he should see that the library access is "([^"]*)"$/)
    void should_see_that_the_library_access_is(String expectedAccess) {
        theActor.shouldEventually(
                seeThat(the(LIBRARY_ACCESS_TOGGLE), isPresent()),
                seeThat(MemberLibraryAccess.value(), equalTo(expectedAccess as AccessLevel))
        )
    }

    @Then(/^s?he should see that the library type is "([^"]*)"$/)
    void should_see_that_the_library_type_is(String expectedType) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(LIBRARY_TYPE), containsString(expectedType))
        )
    }

    @Then(/^s?he should see that the library name is "([^"]*)"$/)
    void should_see_that_the_library_name_is(String expectedName) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(LIBRARY_NAME), equalTo(expectedName))
        )
    }

    @Then(/^s?he should see that the library description is "([^"]*)"$/)
    void should_see_that_the_library_description_is(String expectedDescription) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(LIBRARY_DESCRIPTION), equalTo(expectedDescription))
        )
    }

    @Then(/^s?he should see that the library was created "([^"]*)"$/)
    void should_see_that_the_library_was_created(String expectedText) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(LIBRARY_CREATED_DATE), equalTo(expectedText))
        )
    }

    @Then(/^s?he should see that the library was last modified "([^"]*)"$/)
    void should_see_that_the_library_was_last_modified(String expectedText) {
        theActor.shouldEventually(
                seeThat(TheTarget.textOf(LIBRARY_LAST_MODIFIED_DATE), equalTo(expectedText))
        )
    }

    @Then(/^s?he should see that the delete library button is available$/)
    void should_see_that_the_delete_library_button_is_available() {
        theActor.shouldEventually(
                seeThat(the(LIBRARY_DELETE_BTN), isPresent()),
        )
    }
    
    @Then(/^s?he should see that the library has (\d+) items$/)
    void should_see_that_the_library_has_items(Integer expectedCount) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

}
