/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.model.User
import earthnewmedia.bdd.model.UserRole
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.LoginWithUi
import earthnewmedia.bdd.tasks.NavigateTo
import earthnewmedia.bdd.tasks.ReloadPage
import earthnewmedia.bdd.tasks.StartWith
import earthnewmedia.bdd.tasks.api.InitTestFixtures
import earthnewmedia.bdd.tasks.api.LoginWithApi
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 *
 */
class CommonStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }
    
    @Given(/^that an admin was able to initialize the system under test$/)
    void an_admin_was_able_toinitialize_the_system_under_test() {
        theActor.whoIsNamed("Administrator")
                .whoCan(Authenticate.withCredentials("admin@example.com", 'password0'))
                .whoCanUseTheApi()
                .wasAbleTo(LoginWithApi.asPrincipal())
                .wasAbleTo(InitTestFixtures.usingApi())
    }
    
    @Given(/^that "([^"]*)" is a Guest$/)
    void is_a_guest(String actor) {
        theActor.whoIsNamed(actor).isAGuest()
    }
    
    @Given(/^that s?he was able to start with the landing page$/)
    void was_able_to_start_with_the_landing_page() {
        theActor.wasAbleTo(StartWith.theLandingPage())
    }
    
    @Given(/^that "([^"]*)" is an? (Admin|Member) with the account data$/)
    public void is_an_actor_with_the_account_data(String actor, UserRole role, Map<String, String> principalData) {
        theActor.whoIsNamed(actor)
            .isA(role)
            .whoCanAuthenticateAs(new User(principalData))
    }
    
    @Given(/^s?he was able to login with valid credentials$/)
    public void actor_was_able_to_login_with_valid_credentials() {
        theActor.wasAbleTo(NavigateTo.theLoginPage(), LoginWithUi.usingCredentials())
    }

    @When(/^s?he refreshes the page$/)
    void refreshes_the_page() {
        theActor.attemptsTo(ReloadPage.inBrowser())
    }
    
    @Given(/^the test resources exist in the classpath$/)
    void the_test_resources_exist_in_the_classpath(List<String> expectedResources) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }
}
