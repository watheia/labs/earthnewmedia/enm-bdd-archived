/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs

import earthnewmedia.bdd.steps.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 *
 */
class SystemUiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Then(/^s?he should see the system info message "([^"]*)"$/)
    void should_see_the_system_info_message(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }


}
