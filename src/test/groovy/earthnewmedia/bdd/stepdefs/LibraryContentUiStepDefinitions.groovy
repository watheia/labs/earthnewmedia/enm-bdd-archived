/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.stepdefs

import earthnewmedia.bdd.steps.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 *
 */
class LibraryContentUiStepDefinitions {

    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When(/^s?he attempts to add the items to the album$/)
    void attempts_to_add_the_items_to_the_album(List<Map<String, String>> items) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException()
    }

    @When(/^s?he attempts to navigate to the "([^"]*)" content page$/)
    void attempts_to_navigate_to_the_content_page(String contentSlug) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then(/^s?he should see that he is on the "([^"]*)" content page$/)
    void should_see_that_he_is_on_the_content_page(String fullSlug) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then(/^s?he should see that the content has no comments$/)
    void should_see_that_the_content_has_no_comments() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
}
