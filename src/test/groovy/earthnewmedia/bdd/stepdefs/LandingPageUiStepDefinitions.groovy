/**
 * 
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.HeaderComponent.*
import static earthnewmedia.bdd.ui.LandingPage.HERO_IMG
import static earthnewmedia.bdd.ui.SearchComponent.SEARCH_FORM
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.steps.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LandingPageUiStepDefinitions {
    
    @Steps
    TheActor theActor
    
    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }
    
    @Then(/^s?he should see that they are on the landing page$/)
    void should_see_that_they_are_on_the_landing_page() {
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith("/#/"))
        )
    }

    @Then(/^s?he should see that the login nav is available$/)
    public void should_see_that_the_login_nav_is_available() {
        theActor.should(
                seeThat(the(LOGIN_NAV), isPresent())
        )
    }
    
    @Then(/^s?he should see that the home nav is available$/)
    public void should_see_that_the_home_nav_is_not_available() {
        theActor.should(
                seeThat(the(HOME_NAV), isPresent())
        )
    }

    @Then(/^s?he should see that the login nav is not available$/)
    public void should_see_that_the_login_nav_is_not_available() {
        theActor.should(
                seeThat(the(LOGIN_NAV), isNotPresent())
        )
    }

    @Then(/^s?he should see that the register nav is available$/)
    public void should_see_that_the_register_nav_is_available() {
        theActor.should(
                seeThat(the(REGISTER_NAV), isPresent())
        )
    }

    @Then(/^s?he should see that the register nav is not available$/)
    public void should_see_that_the_register_nav_is_not_available() {
        theActor.should(
                seeThat(the(REGISTER_NAV), isNotPresent())
        )
    }
    
    @Then(/^s?he should see that the search widget is available$/)
    void should_see_that_the_search_widget_is_available() {
        theActor.should(
                seeThat(the(SEARCH_FORM), isPresent())
        )
    }

    @Then(/^s?he should see that the hero image is available$/)
    public void should_see_that_the_hero_image_is_available() {
        theActor.should(
                seeThat(the(HERO_IMG), isPresent())
        )
    }
}
