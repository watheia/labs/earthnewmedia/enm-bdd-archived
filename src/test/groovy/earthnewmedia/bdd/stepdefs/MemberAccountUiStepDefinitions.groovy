/**
 * 
 */
package earthnewmedia.bdd.stepdefs

import static earthnewmedia.bdd.ui.PrincipalUserComponent.*
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.questions.CurrentUrl
import earthnewmedia.bdd.steps.TheActor
import earthnewmedia.bdd.tasks.NavigateTo
import io.cucumber.datatable.DataTable
import io.cucumber.java.Before
import io.cucumber.java.PendingException
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.serenitybdd.screenplay.waits.WaitUntil
import net.thucydides.core.annotations.Shared

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberAccountUiStepDefinitions {

    @Shared
    TheActor theActor

    @Before
    void set_the_stage() {
        OnStage.setTheStage(new OnlineCast())
    }

    @Given(/^s?he is on the member account page$/)
    void is_on_the_member_account_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException()
    }

    @When(/^s?he attempts to navigate to the member account page$/)
    void attempts_to_navigate_to_the_member_account_page() {
        theActor.attemptsTo(
                WaitUntil.the(PRINCIPAL_CARD, isPresent()),
                NavigateTo.ownAccountPage()
        )
    }

    @When(/^s?he attempts to change the user name to "([^"]*)"$/)
    void attempts_to_change_the_user_name_to(String arg1) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException()
    }

    @When(/^s?he attempts to change the to change the user password to$/)
    void attempts_to_change_the_to_change_the_user_password_to(DataTable arg1) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc).
        // Field names for YourType must match the column names in
        // your feature file (except for spaces and capitalization).
        throw new PendingException()
    }

    @Then(/^s?he should see that they are on the member account page$/)
    void should_see_that_they_are_on_the_member_account_page() {
        def principal = Authenticate.asPrincipal(theActor.inTheSpotlight())
        def expectedUrl = "/#/u/${principal.slug}/account"
        theActor.shouldEventually(
                seeThat(CurrentUrl.value(), endsWith(expectedUrl))
        )
    }

    @Then(/^s?he should see that they are on the "([^"]*)" member account page$/)
    void should_see_that_they_are_on_the_member_account_page(String arg1) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException()
    }
}
