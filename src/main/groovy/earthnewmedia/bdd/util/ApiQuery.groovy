/**
 * 
 */
package earthnewmedia.bdd.util

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiQuery {
    
    static String whereQuery(Map params ) {
        final String paramStr = params.inject("") { result, entry ->
            result << "[${entry.key}]=${entry.value}"
        }
        
        return "?where${paramStr}"
    }
}
