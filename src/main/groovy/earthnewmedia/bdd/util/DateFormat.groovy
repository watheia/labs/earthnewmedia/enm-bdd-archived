/**
 * 
 */
package earthnewmedia.bdd.util

import java.text.SimpleDateFormat

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class DateFormat {
    static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    
    static Date parse(String dateStr) {
        return FORMAT.parse(dateStr)
    }
}
