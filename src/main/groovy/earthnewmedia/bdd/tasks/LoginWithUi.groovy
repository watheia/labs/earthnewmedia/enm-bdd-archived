/**
 * 
 */
package earthnewmedia.bdd.tasks

import static earthnewmedia.bdd.ui.LoginComponent.*
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.abilities.Authenticate
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actions.Enter
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LoginWithUi implements Task {

    static LoginWithUi usingCredentials() {
        return instrumented(LoginWithUi.class)
    }

    @Step("{0} logs in with thier credentials")
    <T extends Actor> void performAs(T actor) {
        def model = Authenticate.asPrincipal(actor)
        actor.attemptsTo(
                Enter.theValue(model.email).into(EMAIL_FIELD),
                Enter.theValue(model.password).into(PASSWORD_FIELD),
                Click.on(LOGIN_BUTTON)
        )
    }
}
