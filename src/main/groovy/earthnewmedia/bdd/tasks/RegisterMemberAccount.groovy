/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks

import static earthnewmedia.bdd.ui.RegistrationComponent.*
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.model.RegistrationData
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actions.Enter
import net.thucydides.core.annotations.Step

/**
 * 
 *
 */
class RegisterMemberAccount implements Task {

    static RegisterMemberAccount withModel(RegistrationData model) {
        return instrumented(RegisterMemberAccount.class, model)
    }

    RegistrationData model

    RegisterMemberAccount(RegistrationData model) {
        this.model = model
    }

    @Step("{0} registers an account with #model")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(model.email).into(REGISTRATION_EMAIL_INPUT),
                Enter.theValue(model.userName).into(REGISTRATION_USER_NAME_INPUT),
                Enter.theValue(model.password1).into(REGISTRATION_PASSWD_INPUT_1),
                Enter.theValue(model.password2).into(REGISTRATION_PASSWD_INPUT_2),
                Click.on(REGISTRATION_SUBMIT_BTN)
        )
    }
}
