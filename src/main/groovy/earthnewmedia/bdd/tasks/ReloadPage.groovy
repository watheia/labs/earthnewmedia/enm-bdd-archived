/**
 * 
 */
package earthnewmedia.bdd.tasks

import static net.serenitybdd.core.Serenity.getDriver
import static net.serenitybdd.screenplay.Tasks.instrumented

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Open
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ReloadPage implements Task {

    @Step("Reload the current page")
    static ReloadPage inBrowser() {
        return instrumented(ReloadPage.class)
    }

    @Step
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(getDriver().currentUrl))
    }
}
