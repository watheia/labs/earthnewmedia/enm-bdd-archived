/**
 * 
 */
package earthnewmedia.bdd.tasks

import static earthnewmedia.bdd.ui.HeaderComponent.* 
import static earthnewmedia.bdd.ui.PrincipalUserComponent.PRINCIPAL_ACCOUNT_NAV
import static earthnewmedia.bdd.ui.PrincipalUserComponent.PRINCIPAL_LOGOUT_NAV
import static earthnewmedia.bdd.ui.PrincipalUserComponent.PRINCIPAL_PROFILE_NAV
import static earthnewmedia.bdd.ui.library.MemberLibraryListComponent.memberLibraryListItem

import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Click

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class NavigateTo {
    static Task theLoginPage() {
        return Task.where("Navigate to the login page",
                Click.on(LOGIN_NAV))
    }

    static Task theRegistrationPage() {
        return Task.where("Navigate to the registration page",
                Click.on(REGISTER_NAV))
    }
    
    static Task theLogoutPage() {
        return Task.where("Logout from application",
                Click.on(PRINCIPAL_LOGOUT_NAV))
    }

    static Task ownProfilePage() {
        return Task.where("Navigate to own profile page",
                Click.on(PRINCIPAL_PROFILE_NAV))
    }
    
    static Task ownAccountPage() {
        return Task.where("Navigate to own account page",
                Click.on(PRINCIPAL_ACCOUNT_NAV))
    }
    
    static Task ownHomePage() {
        return Task.where("Navigate to own home page",
                Click.on(HOME_NAV))
    }
    
    static Task theLibraryPage(String slug) {
        return Task.where("Navigate to the library page: ${slug}",
                Click.on(memberLibraryListItem(slug)))
    }
}
