/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks

import static earthnewmedia.bdd.ui.library.MemberLibraryComponent.LIBRARY_ACCESS_TOGGLE
import static net.serenitybdd.screenplay.Tasks.instrumented
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent

import earthnewmedia.bdd.actions.SetSwitch
import earthnewmedia.bdd.model.AccessLevel
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.waits.WaitUntil
import net.thucydides.core.annotations.Step

/**
 * 
 *
 */
class SetLibraryAccess implements Task {

    static SetLibraryAccess to(AccessLevel access) {
        return instrumented(SetLibraryAccess.class, access)
    }

    AccessLevel access

    SetLibraryAccess(AccessLevel access) {
        this.access = access
    }

    @Step("{0} sets library access to #access")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(LIBRARY_ACCESS_TOGGLE, isPresent()),
                SetSwitch.to(access == AccessLevel.Public).on(LIBRARY_ACCESS_TOGGLE)
        )
    }
}
