/**
 * 
 */
package earthnewmedia.bdd.tasks


import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.ui.LandingPage
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Open
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class StartWith implements Task  {
    
    static StartWith theLandingPage() {
        return instrumented(StartWith.class)
    }

    LandingPage landingPage

    @Step("{0} starts on #landingPage")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn().the(landingPage))
    }
}
