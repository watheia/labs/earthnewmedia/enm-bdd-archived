/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks

import static earthnewmedia.bdd.ui.library.CreateLibraryComponent.*
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.actions.SetSwitch
import earthnewmedia.bdd.model.AccessLevel
import earthnewmedia.bdd.model.Library
import earthnewmedia.bdd.model.LibraryType
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Click
import net.serenitybdd.screenplay.actions.Enter
import net.serenitybdd.screenplay.actions.SelectFromOptions
import net.thucydides.core.annotations.Step

/**
 * 
 *
 */
class CreateNewLibrary implements Task {

    static CreateNewLibrary withModel(Library model) {
        return instrumented(CreateNewLibrary.class, model)
    }
    
    Library model
    
    CreateNewLibrary(Library model) {
        this.model = model
    }

    @Step("{0} creates a library with #model")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byValue(model.libraryType.toString())
                        .from(CREATE_LIBRARY_TYPE_SELECT),
                Enter.theValue(model.name).into(CREATE_LIBRARY_NAME_INPUT),
                Enter.theValue(model.description).into(CREATE_LIBRARY_DESCRIPTION_INPUT),
                SetSwitch.to(model.access == AccessLevel.Public)
                        .on(CREATE_LIBRARY_ACCESS_TOGGLE),
                Click.on(CREATE_LIBRARY_SUBMIT_BTN)
        )
    }
}
