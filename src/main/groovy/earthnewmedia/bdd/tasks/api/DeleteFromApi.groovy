/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks.api

import earthnewmedia.bdd.actions.ApiDelete
import earthnewmedia.bdd.actions.ApiInteraction
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.thucydides.core.annotations.Step

/**
 *
 */
class DeleteFromApi extends ApiTask {
    
    DeleteFromApi from(String resource) {
        this.resource = resource
        return this
    }
    
    @Step("{0} attempts to DELETE from api at #resource")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            ApiDelete.from(resource).withRequest({ RequestSpecification req ->
                ApiInteraction.prepareFor(actor, req)
            })
        )
    }
}
