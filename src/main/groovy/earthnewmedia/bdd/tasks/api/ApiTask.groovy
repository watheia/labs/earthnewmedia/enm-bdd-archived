/**
 * 
 */
package earthnewmedia.bdd.tasks.api

import net.serenitybdd.screenplay.Task

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class ApiTask implements Task {
    
    String resource
}
