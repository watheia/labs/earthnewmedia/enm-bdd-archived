/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks.api

import earthnewmedia.bdd.actions.ApiInteraction
import earthnewmedia.bdd.actions.ApiPatch
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.thucydides.core.annotations.Step

/**
 *
 */
class PatchToApi extends ApiTask {
    
    Object payload
    
    PatchToApi to(String resource) {
        this.resource = resource
        return this
    }
    
    PatchToApi withPayload(Object payload) {
        this.payload = payload
        return this
    }
    
    @Step("{0} attempts to PATCH to api at #resource with #payload")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            ApiPatch.to(resource).withRequest({ RequestSpecification req ->
                ApiInteraction.prepareFor(actor, req).body(payload)
            })
        )
    }
}
