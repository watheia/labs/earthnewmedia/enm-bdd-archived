/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks.api

import earthnewmedia.bdd.actions.ApiInteraction
import earthnewmedia.bdd.actions.ApiPost
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.thucydides.core.annotations.Step

/**
 *
 */
class PostToApi extends ApiTask {
    
    Object payload
    
    PostToApi to(String resource) {
        this.resource = resource
        return this
    }
    
    PostToApi withPayload(Object payload) {
        this.payload = payload
        return this
    }
    
    @Step("{0} attempts to POST to api at #resource with #payload")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            ApiPost.to(resource).withRequest({ RequestSpecification req ->
                ApiInteraction.prepareFor(actor, req).body(payload)
            })
        )
    }
}
