/**
 * 
 */
package earthnewmedia.bdd.tasks.api

import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.actions.ApiGet
import earthnewmedia.bdd.actions.ApiInteraction
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class InitTestFixtures implements Task {

    static final String RESOURCE = "/test/init-fixtures"

    static InitTestFixtures usingApi() {
        return instrumented(InitTestFixtures.class)
    }

    @Step("{0} initializes the system under test")
    <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            ApiGet.from(RESOURCE).withRequest({ RequestSpecification req ->
                ApiInteraction.prepareFor(actor, req)
            })
        )
    }
}
