/**
 * 
 */
package earthnewmedia.bdd.tasks.api

import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.actions.ApiInteraction
import earthnewmedia.bdd.actions.ApiPost
import io.restassured.response.Response
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LoginWithApi implements Task {
    
    static LoginWithApi asPrincipal() {
        return instrumented(LoginWithApi.class);
    }

    @Step("{0} logs into api using credentials")
    <T extends Actor> void performAs(T actor) {
        def principal = Authenticate.asPrincipal(actor)
        actor.attemptsTo(
                // NOTE: PostToApi is an alias for Post, renaming `with` to `withRequest`
                // so that Groovy does not attempt to match it to the default `with(Closure closure)`
                ApiPost.to("/login").withRequest({ RequestSpecification req ->
                    ApiInteraction.prepareFor(actor, req, principal)
                        .body([email: principal.email, password: principal.password])
                })
        )
        
        Response lastResponse = LastResponse.received().answeredBy(actor)
        principal.id = lastResponse.jsonPath().getString('userId')
        principal.token = lastResponse.jsonPath().getString('token')
    }
}
