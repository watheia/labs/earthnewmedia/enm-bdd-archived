/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.tasks.api

import static net.serenitybdd.screenplay.Tasks.instrumented

import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.actions.Click

/**
 * Factory class for constructing ApiTask instances
 */
class Api {
    
    static GetFromApi get() {
        return instrumented(GetFromApi)
    }
    
    static DeleteFromApi delete() {
        return instrumented(DeleteFromApi)
    }
    
    static PostToApi post() {
        return instrumented(PostToApi)
    }

    static PatchToApi patch() {
        return instrumented(PatchToApi)
    }
}
