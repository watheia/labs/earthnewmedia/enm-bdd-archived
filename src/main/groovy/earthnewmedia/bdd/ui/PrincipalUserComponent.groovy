/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.core.pages.PageObject
import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class PrincipalUserComponent {

    public static final Target PRINCIPAL_CARD = Target.the("Principal user widget")
            .locatedBy("#enm-principal-card")
            
    public static final Target PRINCIPAL_AVATAR_IMG = Target.the("Principal user's avatar image")
            .locatedBy("#enm-principal-avatar")
            
    public static final Target PRINCIPAL_USERNAME = Target.the("Principal user's username")
            .locatedBy("#enm-principal-username")
            
    public static final Target PRINCIPAL_ACCOUNT_NAV = Target.the("Principal user's account nav")
            .locatedBy("#enm-principal-nav-account")

    public static final Target PRINCIPAL_PROFILE_NAV = Target.the("Principal user's profile nav")
            .locatedBy("#enm-principal-nav-profile")

    public static final Target PRINCIPAL_LOGOUT_NAV = Target.the("Principal user's logout nav")
            .locatedBy("#enm-principal-nav-logout")
}
