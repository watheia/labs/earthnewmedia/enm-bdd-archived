/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.core.pages.PageObject
import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class RegistrationComponent extends PageObject {

    public static Target REGISTRATION_CARD = Target.the("Member registration widget")
        .locatedBy("#enm-registration-card")
        
    public static Target REGISTRATION_EMAIL_INPUT = Target.the("Member registration email input")
        .locatedBy("#enm-registration-email-input")
        
    public static Target REGISTRATION_USER_NAME_INPUT = Target.the("Member registration user name input")
        .locatedBy("#enm-registration-username-input")
        
    public static Target REGISTRATION_PASSWD_INPUT_1 = Target.the("Member registration password 1 input")
        .locatedBy("#enm-registration-password-input-1")
        
    public static Target REGISTRATION_PASSWD_INPUT_2 = Target.the("Member registration password 2 input")
        .locatedBy("#enm-registration-password-input-2")
        
    public static Target REGISTRATION_SUBMIT_BTN = Target.the("Member registration submit button")
        .locatedBy("#enm-registration-submit-btn")
        
    public static Target REGISTRATION_FORM_VALIDATION_MSG = Target.the("Member registration form validation message")
            .locatedBy("#enm-registration-form-validation-msg")
}
