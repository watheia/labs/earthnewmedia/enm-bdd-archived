/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */

public class LoginComponent {
    public static Target LOGIN_FORM = Target.the("Member login form")
        .locatedBy("#enm-login-form")

    public static Target EMAIL_FIELD = Target.the("Email input field")
            .locatedBy("#enm-login-email-input")
    
    public static Target PASSWORD_FIELD = Target.the("Password email field")
            .locatedBy("#enm-login-password-input")
    
    public static Target LOGIN_BUTTON = Target.the("Submit login button")
            .locatedBy("#enm-login-submit-btn")
}
