/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class HeaderComponent {

    public static final Target HOME_NAV = Target.the("Home nav item")
            .locatedBy("#enm-nav-home")
    
    public static final Target LOGIN_NAV = Target.the("Login Nav Item")
            .locatedBy("#enm-nav-login")

    public static final Target REGISTER_NAV = Target.the("Logout Nav Item")
            .locatedBy("#enm-nav-register")
}
