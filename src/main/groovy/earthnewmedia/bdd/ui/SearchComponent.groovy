/**
 * 
 */
package earthnewmedia.bdd.ui

import org.openqa.selenium.WebDriver

import com.google.common.base.Predicate

import net.serenitybdd.core.pages.PageObject
import net.serenitybdd.screenplay.targets.Target
import net.thucydides.core.util.EnvironmentVariables

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class SearchComponent {
    
    public static final Target SEARCH_FORM = Target.the("Search widget form")
            .locatedBy("#enm-search-form")
}
