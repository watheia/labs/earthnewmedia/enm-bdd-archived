/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.core.pages.PageObject
import net.serenitybdd.screenplay.targets.Target
import net.thucydides.core.annotations.DefaultUrl

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@DefaultUrl("http://localhost:3001/#")
public class LandingPage extends PageObject {
    public static final Target HERO_IMG = Target.the("Main Hero Image")
        .locatedBy("#enm-hero-img")
}
