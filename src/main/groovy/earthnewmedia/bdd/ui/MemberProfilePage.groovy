/**
 * 
 */
package earthnewmedia.bdd.ui

import net.serenitybdd.core.pages.PageObject
import net.thucydides.core.annotations.DefaultUrl

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@DefaultUrl("http://localhost:3001/#/user/{1}/profile")
public class MemberProfilePage extends PageObject {}
