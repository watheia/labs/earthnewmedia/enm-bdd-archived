/**
 * 
 */
package earthnewmedia.bdd.ui.library

import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberLibraryListComponent {
    
    public static final Target MEMBER_LIBRARIES_CARD = Target.the("Member library list card")
            .locatedBy("#enm-member-libraries-card")
            
    public static final Target MEMBER_LIBRARY_LIST = Target.the("Member library list")
            .locatedBy("#enm-member-libraries-list")
            
    public static final Target MEMBER_LIBRARY_LIST_ITEMS = Target.the("Member library list items")
            .locatedBy("#enm-member-libraries-list .list-group-item")
            
    public static final Target memberLibraryListItem(String slug) {
        return Target.the("Member libraries list item (${slug})")
            .locatedBy("#enm-member-libraries-list .list-group-item[data-slug=${slug}]")
    }
}
