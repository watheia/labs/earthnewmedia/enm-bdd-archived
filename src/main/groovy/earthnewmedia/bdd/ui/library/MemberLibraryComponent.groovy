/**
 * 
 */
package earthnewmedia.bdd.ui.library

import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class MemberLibraryComponent {
    
    public static final Target LIBRARY_CARD = Target.the("Library card")
            .locatedBy("#enm-library-card")
            
    public static final Target LIBRARY_NAME = Target.the("Library name")
            .locatedBy("#enm-library-name")
            
    public static final Target LIBRARY_TYPE = Target.the("Library type")
            .locatedBy("#enm-library-type")
            
    public static final Target LIBRARY_DESCRIPTION = Target.the("Library description")
            .locatedBy("#enm-library-description")
            
    public static final Target LIBRARY_CREATED_DATE = Target.the("Library created date")
            .locatedBy("#enm-library-created-date span")
            
    public static final Target LIBRARY_LAST_MODIFIED_DATE = Target.the("Library last modified date")
            .locatedBy("#enm-library-last-modified-date span")
            
    public static final Target LIBRARY_DELETE_BTN = Target.the("Library delete button")
            .locatedBy("#enm-library-delete-btn")
            
    public static final Target LIBRARY_ACCESS_TOGGLE = Target.the("Library access input")
            .locatedBy("#enm-library-access-check")
            
}
