/**
 * 
 */
package earthnewmedia.bdd.ui.library

import net.serenitybdd.screenplay.targets.Target

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CreateLibraryComponent {
    
    public static final Target CREATE_LIBRARY_CARD = Target.the("Create new Library widget")
            .locatedBy("#enm-create-library-card")
            
    public static final Target CREATE_LIBRARY_TYPE_SELECT = Target.the("New library type input")
            .locatedBy("#enm-create-library-type-select")
            
    public static final Target CREATE_LIBRARY_NAME_INPUT = Target.the("New library name input")
            .locatedBy("#enm-create-library-name-input")
            
    public static final Target CREATE_LIBRARY_DESCRIPTION_INPUT = Target.the("New library description input")
            .locatedBy("#enm-create-library-desciption-input")
            
    public static final Target CREATE_LIBRARY_ACCESS_TOGGLE = Target.the("New library access input")
            .locatedBy("#enm-create-library-access-check")
            
    public static final Target CREATE_LIBRARY_SUBMIT_BTN = Target.the("New library submit button")
            .locatedBy("#enm-create-library-submit-btn")
}
