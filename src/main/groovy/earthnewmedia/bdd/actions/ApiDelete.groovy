/**
 * 
 */
package earthnewmedia.bdd.actions

import static net.serenitybdd.screenplay.Tasks.instrumented

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.abilities.CallAnApi
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiDelete extends ApiInteraction {

    private final String resource

    public ApiDelete(String resource) {
        this.resource = resource
    }

    @Step("{0} executes a DELETE on the resource #resource")
    @Override
    public <T extends Actor> void performAs(T actor) {
        rest().delete(CallAnApi.as(actor).resolve(resource))
    }

    public static ApiDelete from(String resource) {
        return instrumented(ApiDelete.class, resource)
    }
}
