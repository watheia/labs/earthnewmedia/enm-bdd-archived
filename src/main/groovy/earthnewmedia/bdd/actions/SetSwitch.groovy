/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.actions

import static net.serenitybdd.screenplay.Tasks.instrumented
import static net.serenitybdd.core.Serenity.getDriver

import org.openqa.selenium.JavascriptExecutor

import net.serenitybdd.core.pages.WebElementFacade
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Interaction
import net.serenitybdd.screenplay.targets.Target
import net.thucydides.core.annotations.Step

/**
 * 
 * 
 * The bootstrap switch component doesn't seem to play well with raw
 * WebDriver clicks, so this is class is a work around to set the `checked` 
 * attribute value of the input directly through JavaScript
 */
class SetSwitch implements Interaction {

    static SetSwitch to(Boolean checked) {
        return instrumented(SetSwitch.class, checked)
    }

    Target target

    Boolean checked

    SetSwitch(Boolean checked) {
        this.checked = checked
    }

    @Step("{0} sets checked to #checked on #target")
    <T extends Actor> void performAs(T actor) {
        final WebElementFacade element = target.resolveFor(actor)

        // only update when changing states
        def currentVal = element.getAttribute('checked')
        if((currentVal && !checked) || (!currentVal && checked)) {
            // Tell WebDriver to execute the native script passing our
            // input element as an argument
            ((JavascriptExecutor) getDriver())
                    .executeScript("arguments[0].click();", element)
        }
    }

    SetSwitch on(Target target) {
        this.target = target
        return this
    }
}
