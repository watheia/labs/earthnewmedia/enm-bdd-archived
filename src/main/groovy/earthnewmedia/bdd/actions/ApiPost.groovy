/**
 * 
 */
package earthnewmedia.bdd.actions

import static net.serenitybdd.screenplay.Tasks.instrumented

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.abilities.CallAnApi
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiPost extends ApiInteraction {

    private final String resource

    public ApiPost(String resource) {
        this.resource = resource
    }

    @Step("{0} executes a POST on the resource #resource")
    @Override
    public <T extends Actor> void performAs(T actor) {
        rest().post(CallAnApi.as(actor).resolve(resource))
    }

    public static ApiPost to(String resource) {
        return instrumented(ApiPost.class, resource)
    }
}
