/**
 * 
 */
package earthnewmedia.bdd.actions

import static net.serenitybdd.screenplay.Tasks.instrumented

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.abilities.CallAnApi
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiPatch extends ApiInteraction {

    private final String resource

    public ApiPatch(String resource) {
        this.resource = resource
    }

    @Step("{0} executes a POST on the resource #resource")
    @Override
    public <T extends Actor> void performAs(T actor) {
        rest().patch(CallAnApi.as(actor).resolve(resource))
    }

    public static ApiPatch to(String resource) {
        return instrumented(ApiPatch.class, resource)
    }
}
