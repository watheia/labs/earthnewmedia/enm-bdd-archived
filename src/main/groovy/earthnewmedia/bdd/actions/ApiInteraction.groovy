/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
package earthnewmedia.bdd.actions

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.exceptions.CannotAuthenticateException
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.interactions.RestInteraction
import net.serenitybdd.screenplay.rest.questions.RestQueryFunction

/**
 * We need to implement our own RestInteraction class so not to clash with groovy default methods
 *
 */
abstract class ApiInteraction extends RestInteraction {

    static <T extends Actor> RequestSpecification prepareFor(final T actor, final RequestSpecification req, Authenticate principal = null) {
        req.header("Content-Type", "application/json")
        
        if(!principal) {
            try {
                principal = Authenticate.asPrincipal(actor)
            }
            catch(CannotAuthenticateException e) {
                // swallow error
            }
        }

        if(principal && principal.token) {
            req.header("Authorization", "Bearer ${principal.token}")
        }

        return req
    }

    public RestInteraction withRequest(RestQueryFunction restConfiguration) {
        super.with(restConfiguration)
        return this
    }

    public RestInteraction withRequest(List<RestQueryFunction> restConfigurations) {
        super.with(restConfigurations)
        return this
    }
}
