/**
 * 
 */
package earthnewmedia.bdd.model

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
enum UserRole {
    Admin, Support, Member, Guest
}
