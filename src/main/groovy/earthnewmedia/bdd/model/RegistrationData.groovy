/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 * 
 */
package earthnewmedia.bdd.model

import groovy.transform.Canonical

/**
 *
 */
@Canonical
class RegistrationData {
    String email
    String userName
    String password1
    String password2
}
