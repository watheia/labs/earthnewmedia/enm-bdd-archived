/**
 * 
 */
package earthnewmedia.bdd.model

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
enum LibraryType {
    Discussion,
    Album,
    Gallery
}
