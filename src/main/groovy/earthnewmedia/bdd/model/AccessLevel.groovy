package earthnewmedia.bdd.model

enum AccessLevel {
    Public,
    Private
}
