/**
 * 
 */
package earthnewmedia.bdd.model

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class Library {
    AccessLevel access
    LibraryType libraryType
    String name
    String description
}
