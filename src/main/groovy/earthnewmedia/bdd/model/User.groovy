/**
 * 
 */
package earthnewmedia.bdd.model

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class User {
    String id
    String token
    String slug
    String email
    String userName
    String password
}
