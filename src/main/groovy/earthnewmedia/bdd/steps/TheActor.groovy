/**
 * 
 */
package earthnewmedia.bdd.steps

import static net.serenitybdd.screenplay.EventualConsequence.eventually
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight

import earthnewmedia.bdd.abilities.Authenticate
import earthnewmedia.bdd.model.User
import earthnewmedia.bdd.model.UserRole
import earthnewmedia.bdd.tasks.api.LoginWithApi
import net.serenitybdd.screenplay.Ability
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Consequence
import net.serenitybdd.screenplay.Performable
import net.serenitybdd.screenplay.rest.abilities.CallAnApi
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class TheActor {
    
    String actor
    
    Actor inTheSpotlight() {
        return theActorInTheSpotlight()
    }

    @Step("{1} is an actor using the system under test")
    TheActor whoIsNamed(String actor) {
        theActorCalled(actor)
        return this
    }

    @Step("#actor is a Guest")
    TheActor isAGuest() {
        return this
    }
    
    @Step("#actor is a {0}")
    TheActor isA(UserRole role) {
        return this
    }

    @Step("#actor has an account with {0}")
    TheActor whoHasTheAccount(Map accountData) {
        return this
    }

    @Step("#actor can authenticate as {0}")
    TheActor whoCanAuthenticateAs(User principal) {
        theActorInTheSpotlight()
                .whoCan(Authenticate.asPrincipal(principal))
        
        return this
    }
    
    @Step("#actor can call the rest api")
    TheActor whoCanUseTheApi() {
        theActorInTheSpotlight()
                .whoCan(CallAnApi.at("http://localhost:3000"))
        
        return this
    }
    
    TheActor wasAbleTo(Performable... todos) {
        theActorInTheSpotlight().wasAbleTo(todos)
        
        return this
    }
    
    TheActor attemptsTo(Performable... tasks) {
        theActorInTheSpotlight().attemptsTo(tasks)
        
        return this
    }

    TheActor shouldEventually(Consequence... consequences) {
        consequences.each {            
            theActorInTheSpotlight().should(eventually(it))
        }

        return this
    }
    
    TheActor should(Consequence... consequences) {
        theActorInTheSpotlight().should(consequences)

        return this
    }
    
    public <T extends Ability> TheActor whoCan(T doSomething) {
        theActorInTheSpotlight().whoCan(doSomething)
        return this
    }
}
