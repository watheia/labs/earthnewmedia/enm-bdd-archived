/**
 * 
 */
package earthnewmedia.bdd.steps

import static earthnewmedia.bdd.util.ApiQuery.whereQuery
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight

import earthnewmedia.bdd.tasks.api.Api
import earthnewmedia.bdd.tasks.api.ApiTask
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UserAttributeApiSteps {
    
    private String actor
    
    @Step("#actor creates the user attribute")
    ApiTask create(String label, String content) {
        return Api.post()
                .to("/user-attribute")
                .withPayload([label: label, content: content])
    }
    
    @Step("#actor deletes the user attributes")
    ApiTask delete(Map<String, String> filter) {
        return Api.delete()
                .from("/user-attribute${whereQuery(filter)}")
    }
    
    @Step("#actor counts the user attributes")
    ApiTask count(Map<String, String> filter) {
        return Api.get()
                .from("/user-attribute/count${whereQuery(filter)}")
    }
    
    @Step("#actor finds the user attributes")
    ApiTask find(Map<String, String> filter) {
        return Api.get()
                .from("/user-attribute${whereQuery(filter)}")
    }
    
    @Step("#actor updates the user attribute")
    ApiTask update(Map entry) {
        // get the last user attribute that was created in prv step and update the specified fields from the data
        def lastEntity = LastResponse.received()
                .answeredBy(theActorInTheSpotlight()).jsonPath().get() as Map
        def payload = lastEntity.collectEntries{ k, v -> [(k): entry.get(k) ?: v] }
        
        return Api.patch()
                .to("/user-attribute/${payload.id}")
                .withPayload(payload)
    }
}
