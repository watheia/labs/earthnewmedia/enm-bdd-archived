/**
 * 
 */
package earthnewmedia.bdd.questions

import io.restassured.response.Response
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.annotations.Subject
import net.serenitybdd.screenplay.rest.questions.LastResponse

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Subject("the response entries")
class ApiResponseEntries<T> implements Question<List<T>> {

    Class<T> type

    ApiResponseEntries(Class<T> type) {
        this.type = type
    }

    @Override
    public List<T> answeredBy(Actor actor) {
        return LastResponse.received().answeredBy(actor).jsonPath().get() as List<T>
    }
}
