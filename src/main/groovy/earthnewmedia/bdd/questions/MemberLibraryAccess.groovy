/**
 * 
 */
package earthnewmedia.bdd.questions

import static earthnewmedia.bdd.ui.library.MemberLibraryComponent.LIBRARY_ACCESS_TOGGLE

import earthnewmedia.bdd.model.AccessLevel
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
public class MemberLibraryAccess implements Question<AccessLevel> {

    @Override
    public AccessLevel answeredBy(Actor actor) {
        return LIBRARY_ACCESS_TOGGLE.resolveFor(actor).getAttribute('checked')?
                AccessLevel.Public : AccessLevel.Private
    }

    public static MemberLibraryAccess value() {
        return new MemberLibraryAccess()
    }
}
