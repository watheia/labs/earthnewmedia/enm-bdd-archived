/**
 * 
 */
package earthnewmedia.bdd.questions

import static net.serenitybdd.core.Serenity.getDriver

import org.openqa.selenium.WebDriver

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.annotations.Subject

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Subject("Active Page in History")
class CurrentUrl implements Question<String> {

    @Override
    public String answeredBy(Actor actor){
        WebDriver driver = getDriver()
        return driver.getCurrentUrl()
    }
    public static CurrentUrl value(){
        return new CurrentUrl()
    }
}
