/**
 * 
 */
package earthnewmedia.bdd.questions

import io.restassured.response.Response
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.annotations.Subject
import net.serenitybdd.screenplay.rest.questions.LastResponse

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Subject("the response field")
class ApiResponseBody<T> implements Question<T> {

    String path

    Class<T> type

    Closure<T> parser

    ApiResponseBody(String path, Class<T> type, Closure<T> parser = null) {
        this.path = path
        this.type = type
        this.parser = parser
    }

    @Override
    public T answeredBy(Actor actor) {
        // Use parser if provided, otherwise cast type directly
        final Response lastResponse = LastResponse.received().answeredBy(actor)
        return parser? parser(lastResponse.jsonPath().getString(path)) :
                lastResponse.jsonPath().getObject(path, type)
    }
}
