/**
 * 
 */
package earthnewmedia.bdd.questions

import static earthnewmedia.bdd.ui.library.MemberLibraryListComponent.MEMBER_LIBRARY_LIST_ITEMS

import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.annotations.Subject
import net.serenitybdd.screenplay.questions.Attribute

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Subject("the member library list item slugs")
class TheLibraryList implements Question<List<String>> {

    static TheLibraryList items() {
        return new TheLibraryList()
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Attribute.of(MEMBER_LIBRARY_LIST_ITEMS).named("data-slug")
                .viewedBy(actor)
                .asList()
    }
}
