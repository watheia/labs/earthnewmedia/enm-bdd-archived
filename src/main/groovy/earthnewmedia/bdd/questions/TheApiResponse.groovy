/**
 * 
 */
package earthnewmedia.bdd.questions

import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.rest.questions.TheResponseStatusCode

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class TheApiResponse {

    public static Question<Integer> theResponseStatus() {
        return new TheResponseStatusCode()
    }
    
    public static <T> Question<List<T>> theResponseEntries(Class<T> type = Map) {
        return new ApiResponseEntries(type)
    }

    public static <T> Question<T> theResponseBody(String path, Class<T> type) {
        return new ApiResponseBody(path, type)
    }
    
    public static <T> Question<T> theResponseBody(String path, Closure<T> parser) {
        return new ApiResponseBody(path, T, parser)
    }

    public static Question<String> theResponseBody(String path) {
        return new ApiResponseBody(path, String)
    }
}
