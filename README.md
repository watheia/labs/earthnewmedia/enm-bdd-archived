# earthnew.media

*This is a work in progress*. See the Road Map below or the latest [Serenity BDD Test Report](https://earthnewmedia.gitlab.io/enm-bdd) for more details.

**Phase**: `p1-co1-1`

**Release**: `v0.1a`

## Goals

*earthnew.media* is a proof of concept implementation for a federated social networking site with a focus
on sharing user generated media content. The main premise is based on new crypto-currency tied to the 
platform called EnmCoin (*Ec*). Joining the social network involves an initial minimum *Ec* purchase of X, 
of which an amount Y must be held in the account to remain active. *Ec* can be used to support independent 
artists by setting the smallest *Ec* denomination to a single "Like." Liking a piece of content, a post, or
anything "Likeable" will take one *Ec* from the "Liker" and places it in the account of the "Liked". Certain
items like original video or audio content may have an *Ec* value attached to it in for a single viewing, or 
possibly an amount to own the rights to download and play the content indefinitely.

## Projects

The following projects provide the implementation and tooling for the `earthnewmedia` group

- [enm-bdd](https://gitlab.com/earthnewmedia/enm-bdd): Serenity BDD Test Suite
- [enm-model](https://gitlab.com/earthnewmedia/enm-model): EMF/ECore Modeling Projects
- [enm-api](https://gitlab.com/earthnewmedia/enm-api): Loopback 4 Server Application
- [enm-ui](https://gitlab.com/earthnewmedia/enm-ui): React/Redux Client Application

## Road Map

Each phase will be broken into at least 2 rounds of a [Unified Software Development Process](https://en.wikipedia.org/wiki/Unified_Process). A stable version will be prepared for release at the end of each phase.

### Phase I

Basic UX and API functionality for a simple media-focused social networking site. 

- `v0.1a` Alpha release with focus on getting "pen to paper"
- `v0.1b` Beta release with focus on improving UX, stability, and performance
- `v0.1.0` MVP reference implementation establishing basic contracts between client and server APIs

### Phase II

Design and implement the network federation protocols, such as account synchronization, content hosting, and decentralized trust

- `v0.2a` *TBD*
- `v0.2b`*TBD*
- `v0.2.0` *TBD*

### Phase III

Establish EnmCoin ICO, and implement related functionality in application

- `v0.3a` *TBD*
- `v0.3b` *TBD*
- `v1.0.0` *TBD*
